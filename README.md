# Code test for Till payments

## Some notes

Usually I would remove alot of the extra stuff that comes along with using the templates, (such as ejecting create-react-app and cleaning up the pre defined contollers/services in nest.js). This was not done to save time.

There is very little if any validation performed throughout the client side or server side. I would normally ensure that any user input is validated on the client side for a good user experiance and on the server side to ensure validity of the data.

With more time and knowledge of the serverside technologies, I would have a database setup and appropriate forign key validation for each of the models with references to other models.

I had decided to have the react app served by its own server to more closely model a microservice architecture over creating a monolith type project, however as can be seen, both projects have been colocated in the same repo.

Due to my decicion above relating to the seperation of projects, I had run out of time for finding a solution to sharing the TS definitions. The solution to me would be to locate the definition on the server side as that is the closest to the model creation and definition.

Tests had only been written for the server side. I had run out of time to complete tests for the client side, but usually I would write unit tests for function and components. Component tests for me are black box style, based around the user interaction (ie, click this button, see this effect).

Due to the basic usecase of this app, I had opted to not include librarys such as `react-router` or `formik` for route and form state managment, to aid in faster development.

## Running the services

From the `server` directory run `yarn start`. This will run on port `3000`.
From the `client` directory run `yarn start`. This will run on port `3001`.

## Running the tests

From the `server` directory run `yarn test`.
Tests were not written for the `client` directory
