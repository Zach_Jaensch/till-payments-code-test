import React, { ReactNode } from 'react'

interface ButtonPropsType {
  onClick?: () => void
  children: ReactNode
  type?: 'button' | 'submit' | 'reset'
}

export function Button({ 
  onClick,
  children, 
  type = 'button'
}: ButtonPropsType) {
  return (
    <button
      type={type}
      className="c-button"
      onClick={onClick}
    >
      {children}
    </button>
  )
}