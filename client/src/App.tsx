import React, { ReactNode, useState } from 'react';
import './App.css';
import { Button } from './Components/Button';
import { MerchantsPage } from './Pages/Merchants';
import { CustomersPage } from './Pages/Customers';
import { CustomerPage } from './Pages/Customer';
import { MerchantPage } from './Pages/Merchant';
import { RouteContext, pagesType } from './Contexts/Route';
import { CreateCustomerPage } from './Pages/CreateCustomer';


const pagesMap: Record<pagesType, ReactNode> = {
  customers: CustomersPage,
  merchants: MerchantsPage,
  merchant: MerchantPage,
  customer: CustomerPage,
  createCustomer: CreateCustomerPage
}


function App() {
  const [currentPage, setCurrentPage] = useState<pagesType>('merchants')
  const [currentId, setCurrentId] = useState<number>()

  const Page: ReactNode = pagesMap[currentPage] || 'div'

  return (
    <RouteContext.Provider 
      value={{
        currentPage,
        setCurrentPage,
        currentId,
        setCurrentId
      }}
    >
      <div className="App">
        <header className="App-header">
          <h1>Till payments code test</h1>
          <h2>By Zach Jaensch</h2>
          <nav>
            <ul className="c-navigation">
              <li className="c-navigation__item"><Button onClick={() => setCurrentPage('merchants')}>Merchants</Button></li>
              <li className="c-navigation__item"><Button onClick={() => setCurrentPage('customers')}>Customers</Button></li>
            </ul>
          </nav>

        </header>
        <section className="c-section">
          {/* This is always going to be a component or element */}
          {/* @ts-ignore */}
          <Page />
        </section>
      </div>
    </RouteContext.Provider>
  );
}

export default App;
