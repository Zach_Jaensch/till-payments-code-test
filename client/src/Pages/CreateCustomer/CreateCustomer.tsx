import React, { SyntheticEvent, useEffect, useState } from "react"
import { Button } from "../../Components/Button"
import { createCustomer } from "../../utils/createCustomer"
import { Merchant } from "../../utils/getMerchant"
import { getMerchants } from "../../utils/getMerchants"

export function CreateCustomerPage() {
  const [merchants, setMerchants] = useState<Merchant[]>([])

  const [merchantId, setMerchantId] = useState<string>('')
  const [name, setName] = useState<string>('')
  const [isComplete, setIsComplete] = useState<boolean>(false)

  function handleSubmit(e: SyntheticEvent) {
    e.preventDefault()
    createCustomer({ merchantId: Number(merchantId), name }, () => {setIsComplete(true)})
  }


  useEffect(() => {
    getMerchants(setMerchants)
  }, [])


  return (
    <>
      <h3>Add new customer</h3>
      {isComplete ? 
        <p>{name} successfully added!</p>
      : (
        <form onSubmit={handleSubmit}>
          <label>
            Merchant
            <select 
              value={merchantId} 
              onChange={({target: { value } }) => {
                setMerchantId(value)
              }}
              required
            >
              <option value="">Select a merchant</option>

              {merchants.map((merchantSelect) => {
                return (
                  <option value={merchantSelect.id}>{merchantSelect.name}</option>
                )
              })}
            </select>
          </label>
          <label>
            Name
            <input 
            type="text" 
            value={name} 
            onChange={({target: { value } }) => {setName(value)}}
            required 
            />
          </label>
          <Button type='submit'>Save</Button>
        </form>
      )

    }
  </>
)
}