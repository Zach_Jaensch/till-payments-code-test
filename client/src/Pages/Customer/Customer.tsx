import React, { useEffect, useState } from "react"
import { useRoute } from "../../Contexts/Route"
import { currencySymbolMap } from "../../utils/currency"
import { Customer, getCustomer } from "../../utils/getCustomer"
import { Merchant } from "../../utils/getMerchant"
import { getMerchant } from "../../utils/getMerchant"

export function CustomerPage() {
  const [customer, setCustomer] = useState<Customer>()
  const router = useRoute()
  const [merchant, setMerchant] = useState<Merchant>()

  useEffect(() => {
    if(!router.currentId) throw new Error('id has not been provided') 
    getCustomer(router.currentId, setCustomer)
  }, [router.currentId])

  useEffect(() => {
    if(customer?.merchantId) {
      getMerchant(customer?.merchantId, setMerchant)
    }
  }, [customer?.merchantId])

  const customerTransactions = (merchant?.transactions || []).filter((transaction) => {
    return transaction.customerId === customer?.id
  }) || []


  return (
    <>
      <h3>{customer?.name}</h3>
      <h4>{merchant?.name}</h4>
      <table>
      <thead>
        <tr>
          <th>Description</th>
          <th>Amount</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        {customerTransactions.map((transaction) => {
          return (
            <tr key={transaction.id}>
              <td>{transaction.description}</td>
              <td>{currencySymbolMap[merchant?.currency || 'AUD']}{transaction.amount / 100} </td>
              <td>{new Date(transaction.date).toLocaleDateString()}</td>
            </tr>
          )
        })}
      </tbody>
    </table>
  </>
)
}