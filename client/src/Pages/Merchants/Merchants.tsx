import React, { useEffect, useState } from "react"
import { Button } from "../../Components/Button"
import { useRoute } from "../../Contexts/Route"
import { currencySymbolMap } from "../../utils/currency"
import { Merchant } from "../../utils/getMerchant"
import { getMerchants } from "../../utils/getMerchants"

export function MerchantsPage() {
  const [merchants, setMerchants] = useState<Merchant[]>([])
  const router = useRoute()
  useEffect(() => {
    getMerchants(setMerchants)
  }, [])

  function handleViewTransaction(id: number) {
    router.setCurrentPage('merchant')
    router.setCurrentId(id)
  }
  
  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Trading</th>
          <th>Currency</th>
          <th>Transactions</th>
        </tr>
      </thead>
      <tbody>
        {merchants.map((merchant) => {
          return (
            <tr key={merchant.id}>
              <td>{merchant.name}</td>
              <td>{merchant.isTrading ? 'Y' : 'N'}</td>
              <td>{merchant.currency} ({currencySymbolMap[merchant.currency]})</td>
              <td><Button onClick={() => {handleViewTransaction(merchant.id)}}>View transactions</Button></td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}