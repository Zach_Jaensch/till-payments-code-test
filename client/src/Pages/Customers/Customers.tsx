import React, { useEffect, useState } from "react"
import { Button } from "../../Components/Button"
import { useRoute } from "../../Contexts/Route"
import { currencySymbolMap } from "../../utils/currency"
import { Customer } from "../../utils/getCustomer"
import { getCustomers } from "../../utils/getCustomers"
import { Merchant } from "../../utils/getMerchant"
import { getMerchants } from "../../utils/getMerchants"

export function CustomersPage() {
  const [customers, setCustomers] = useState<Customer[]>([])
  const router = useRoute()
  useEffect(() => {
    getCustomers(setCustomers)
  }, [])

  const [merchants, setMerchants] = useState<Merchant[]>([])
  useEffect(() => {
    getMerchants(setMerchants)
  }, [])

  function handleViewTransaction(id: number) {
    router.setCurrentPage('customer')
    router.setCurrentId(id)
  }

  return (
    <>
      <Button onClick={() => router.setCurrentPage('createCustomer')}>Add new customer</Button>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Merchant</th>
            <th>Total spent</th>
            <th>Transactions</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer) => {
            const customerMerchant = merchants.find((merchant) => {
              return merchant.id === customer.merchantId
            })
            const customerTransaction = (customerMerchant?.transactions || []).filter((transaction) => {
              return transaction.customerId === customer.id
            })
            const totalSpent = customerTransaction.reduce((total, transaction) => {
              return total + transaction.amount
            }, 0)
            return (
              <tr key={customer.id}>
                <td>{customer.name}</td>
                <td>{customerMerchant?.name}</td>
                <td>{currencySymbolMap[customerMerchant?.currency || 'AUD']}{totalSpent / 100}</td>
                <td><Button onClick={() => {handleViewTransaction(customer.id)}}>View transactions</Button></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}