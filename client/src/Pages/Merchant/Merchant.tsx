import React, { useEffect, useState } from "react"
import { useRoute } from "../../Contexts/Route"
import { getMerchant, Merchant } from "../../utils/getMerchant"
import { currencySymbolMap } from "../../utils/currency"
import { getMerchants } from "../../utils/getMerchants"

export function MerchantPage() {
  const router = useRoute()

  const [merchants, setMerchants] = useState<Merchant[]>([])
  const [merchant, setMerchant] = useState<Merchant>()

  useEffect(() => {
    if(!router.currentId) throw new Error('id has not been provided') 
    getMerchants(setMerchants)
    getMerchant(router.currentId, setMerchant)
  }, [router.currentId])

  useEffect(() => {
    getMerchants(setMerchants)
  }, [])


  return (
    <>
      <h3>{merchant?.name}</h3>
      <form>
        <select value={router.currentId} onChange={({target: { value } }) => {
          router.setCurrentId(Number(value))
        }}>
          {merchants.map((merchantSelect) => {
            return (
              <option value={merchantSelect.id}>{merchantSelect.name}</option>
            )
          })}
        </select>
      </form>
      <table>
        <thead>
          <tr>
            <th>Description</th>
            <th>Amount</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          {(merchant?.transactions || []).map((transaction) => {
            return (
              <tr key={transaction.id}>
                <td>{transaction.description}</td>
                <td>{currencySymbolMap[merchant?.currency || 'AUD']}{transaction.amount / 100} </td>
                <td>{new Date(transaction.date).toLocaleDateString()}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}