export type currencyCodeType = 'AUD' | 'EURO'

export const currencySymbolMap: Record<currencyCodeType, string> = {
  AUD: '$',
  EURO: '€',
}
