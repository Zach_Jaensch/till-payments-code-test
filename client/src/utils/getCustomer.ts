import { currencyCodeType } from "./currency";
import { Merchant, Transaction } from "./getMerchant";
import { request } from "./request";

export interface Customer {
  id: number
  merchantId: number
  name: string
}

export function getCustomer(id: number, cbs: (customer: Customer) => void) {
  request<Customer>({
    method: 'GET',
    route: 'customers/' + id,
  }).then(cbs)
}