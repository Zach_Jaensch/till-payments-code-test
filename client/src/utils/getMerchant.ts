import { currencyCodeType } from "./currency";
import { request } from "./request";

export interface Transaction {
  id: number
  amount: number
  description: string
  ccLastFour: string
  ccExpiry: string
  ccToken: string
  date: Date
  customerId: number
}

export interface Merchant {
  id: number
  name: string
  isTrading: boolean
  currency: currencyCodeType
  transactions: Transaction[]
}

export function getMerchant(id: number,cbs: (merchants: Merchant) => void) {
  request<Merchant>({
    method: 'GET',
    route: 'merchants/' + id,
  }).then(cbs)
}