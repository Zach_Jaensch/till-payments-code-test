import { request } from "./request";

export interface Customer {
  id: number
  merchantId: number
  name: string
}


export interface NewCustomer {
  merchantId: number
  name: string
}

export function createCustomer(newCustomer: NewCustomer, cbs: (customer: Customer) => void) {
  request<Customer>({
    method: 'POST',
    route: 'customers',
    data: {customer: newCustomer},
  }).then(cbs)
}