
interface IRequest {
  method: 'GET' | 'PUT' | 'POST' | 'DELETE'
  route: string
  data?: Record<string, any>
}

export function request<T>({method, route, data}: IRequest): Promise<T> {
  return new Promise((resolve, reject) => {
    const Http = new XMLHttpRequest();
    Http.onload = () => {
      if (Http.status >= 200 && Http.status <= 299) {
        resolve(JSON.parse(Http.responseText))
      } else {
        reject()
      }
    }

    const url = `http://localhost:3000/${route}`
  
    Http.open(method, url);
    if(data) {
      Http.setRequestHeader("Content-Type", "application/json");
      Http.send(JSON.stringify(data))
    } else {
      Http.send()
    }
  });

}
