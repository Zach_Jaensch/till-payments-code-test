import { currencyCodeType } from "./currency";
import { Merchant } from "./getMerchant";
import { request } from "./request";

export function getMerchants(cbs: (merchants: Merchant[]) => void) {
  request<Merchant[]>({
    method: 'GET',
    route: 'merchants',
  }).then(cbs)
}