import { currencyCodeType } from "./currency";
import { Customer } from "./getCustomer";
import { request } from "./request";


export function getCustomers(cbs: (merchants: Customer[]) => void) {
  request<Customer[]>({
    method: 'GET',
    route: 'customers',
  }).then(cbs)
}