import { createContext, useContext } from "react"

export type pagesType = 'customers' | 'customer' | 'createCustomer' | 'merchants' | 'merchant'

interface RouteContextType {
  currentPage?: string
  setCurrentPage: (newPage: pagesType) => void
  currentId?: number
  setCurrentId: (newId: number) => void
}

export const RouteContext = createContext<RouteContextType>({
  currentPage: 'merchants',
  setCurrentPage: () => {},
  currentId: undefined,
  setCurrentId: () => {},
})

export function useRoute() {
  return useContext(RouteContext)
} 
