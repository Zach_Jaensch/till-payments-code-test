import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersController, MerchantsController } from './Controllers';
import { CustomersService, MerchantsService } from './Services';

@Module({
  imports: [],
  controllers: [AppController, CustomersController, MerchantsController],
  providers: [AppService, CustomersService, MerchantsService],
})
export class AppModule {}
