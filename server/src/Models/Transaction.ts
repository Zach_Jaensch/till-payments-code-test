export interface createTransaction {
  amount: number
  description: string
  ccLastFour: string
  ccExpiry: string
  ccToken: string
  date: Date
  customerId: number
}
interface newTransaction extends createTransaction {
  id: number
}


export class Transaction {
  id: number
  amount: number
  description: string
  ccLastFour: string
  ccExpiry: string
  ccToken: string
  date: Date
  customerId: number

  constructor(transaction: newTransaction) {
    this.id = transaction.id
    this.amount = transaction.amount
    this.description = transaction.description
    this.ccLastFour = transaction.ccLastFour
    this.ccExpiry = transaction.ccExpiry
    this.ccToken = transaction.ccToken
    this.date = transaction.date
    this.customerId = transaction.customerId
  }  
}

