export interface createCustomer {
  merchantId: number
  name: string
}
interface newCustomer extends createCustomer {
  id: number
}

export interface updateCustomer {
  merchantId?: number
  name?: string
} 
export class Customer {
  id: number
  merchantId: number
  name: string

  constructor(newCustomer: newCustomer) {
    this.id = newCustomer.id
    this.merchantId = newCustomer.merchantId
    this.name = newCustomer.name
  }
 
  update(data: updateCustomer) {
    this.merchantId = data.merchantId || this.merchantId
    this.name = data.name || this.name
  }
}