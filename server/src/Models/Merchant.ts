import { Transaction } from "./Transaction"

export interface createMerchant {
  name: string
  isTrading: boolean
  currency: string
  transactions: Transaction[]
}
interface newMerchant extends createMerchant {
  id: number
}

export interface updateMerchant {
  name?: string
  isTrading?: boolean
  currency?: string
} 

export class Merchant {
  id: number
  name: string
  isTrading: boolean
  currency: string
  transactions: Transaction[]

  constructor(newMerchant: newMerchant) {
    this.id = newMerchant.id
    this.name = newMerchant.name
    this.isTrading = newMerchant.isTrading
    this.currency = newMerchant.currency
    this.transactions = newMerchant.transactions
  }

  update(data: updateMerchant) {
    this.name = data.name || this.name
    this.isTrading = data.isTrading || this.isTrading
    this.currency = data.currency || this.currency
  }
}