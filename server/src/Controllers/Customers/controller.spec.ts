import { Test, TestingModule } from '@nestjs/testing';
import { CustomersController } from './controller';
import { CustomersService } from '../../Services/customers/service';
import { Customer } from '../../Models/Customer';

const mockCustomers = [
  new Customer({
    "id": 1,
    "merchantId": 1,
    "name": "Alfred Jones"
  }),
  new Customer({
    "id": 2,
    "merchantId": 1,
    "name": "Sally Brian"
  }),
]


describe('CustomersController', () => {
  let controller: CustomersController;
  let service: CustomersService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [CustomersController],
      providers: [CustomersService],
    }).compile();

    controller = app.get<CustomersController>(CustomersController);
    service = app.get<CustomersService>(CustomersService);
  });

  describe('get all', () => {
    it('return the list"', () => {
      jest.spyOn(service, 'getCustomers').mockImplementation(() => mockCustomers);
      expect(controller.getCustomers()).toEqual(mockCustomers);
    });
  });

  describe('get one', () => {
    it('return the list"', () => {
      jest.spyOn(service, 'getCustomer').mockImplementation(() => mockCustomers[1]);
      expect(controller.getCustomer(2)).toEqual(mockCustomers[1]);
    });
  });

  describe('post', () => {
    it('return the list"', () => {
      const newCustomer = {
        "merchantId": 1,
        "name": "James Frank"
      }

      const expected = [...mockCustomers, new Customer({
        "id": 3,
        "merchantId": 1,
        "name": "James Frank"
      })]

      jest.spyOn(service, 'addCustomer').mockImplementation(() => expected);

      expect(controller.addCustomer(newCustomer)).toEqual(expected);
    });
  });

  describe('patch', () => {
    it('return the list"', () => {
      const updateCustomer = {
        "name": "James Frank"
      }
      
      const expected = new Customer({
        "id": 1,
        "merchantId": 1,
        "name": "James Frank"
      })

      jest.spyOn(service, 'updateCustomer').mockImplementation(() => expected);

      expect(controller.updateCustomer(1, updateCustomer)).toEqual(expected);
    });
  });

  describe('delete', () => {
    it('return the list"', () => {
      jest.spyOn(service, 'deleteCustomer').mockImplementation(() => mockCustomers);

      expect(controller.removeCustomer(1)).toEqual(mockCustomers);
    });
  });
});
