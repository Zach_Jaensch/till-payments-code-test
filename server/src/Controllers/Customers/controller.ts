import { Controller, Get, Param, Patch, Post, Body, Delete, ParseIntPipe } from '@nestjs/common';
import { Customer, createCustomer, updateCustomer } from '../../Models/Customer';
import { CustomersService } from '../../Services/customers/service';

@Controller('customers')
export class CustomersController {
  constructor(private readonly service: CustomersService) {}

  @Get()
  getCustomers(): Customer[] {
    return this.service.getCustomers();
  }

  @Get(':id')
  getCustomer(@Param('id', ParseIntPipe) id: number): Customer {
    return this.service.getCustomer(id);
  }

  @Post()
  addCustomer(@Body('customer') customer: createCustomer): Customer[] {
    return this.service.addCustomer(customer);
  }

  @Patch(':id')
  updateCustomer(@Param('id', ParseIntPipe) id: number, @Body('customer') customer: updateCustomer): Customer {
    return this.service.updateCustomer(Number(id), customer);
  }

  @Delete(':id')
  removeCustomer(@Param('id', ParseIntPipe) id: number): Customer[] {
    return this.service.deleteCustomer(Number(id));
  }
}
