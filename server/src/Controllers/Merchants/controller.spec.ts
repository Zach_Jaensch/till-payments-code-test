import { Test, TestingModule } from '@nestjs/testing';
import { MerchantsController } from './controller';
import { MerchantsService } from '../../Services/Merchants/service';
import { Merchant } from '../../Models/Merchant';
import { Transaction } from '../../Models/Transaction';

const mockMerchants = [
  new Merchant({
    "id": 1,
    "name": "Barney's Deals on Wheels",
    "isTrading": true,
    "currency": "AUD",
    transactions: [
      new Transaction({
        "id": 1,
        "amount": 2000,
        "description": "Minor Service",
        "ccLastFour": "4234",
        "ccExpiry": "11/04",
        "ccToken": "GH:D54356JJ$#2>GERG",
        "customerId": 1,
        "date": new Date("2020-09-21 04:56:58.701972"),
      })
    ]
  }),
  new Merchant({
    "id": 2,
    "name": "Nurburgring Laps",
    "isTrading": true,
    "currency": "EURO",
    transactions: [
      new Transaction({
        "id": 1,
        "amount": 2000,
        "description": "1 Lap",
        "ccLastFour": "4234",
        "ccExpiry": "11/04",
        "ccToken": "GH:D54356JJ$#2>GERG",
        "customerId": 4,
        "date": new Date("2020-10-21 02:56:58.701972"),
      })
    ]
  }),
]


describe('MerchantsController', () => {
  let controller: MerchantsController;
  let service: MerchantsService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [MerchantsController],
      providers: [MerchantsService],
    }).compile();

    controller = app.get<MerchantsController>(MerchantsController);
    service = app.get<MerchantsService>(MerchantsService);
  });

  describe('get all', () => {
    it('return the list"', () => {
      jest.spyOn(service, 'getMerchants').mockImplementation(() => mockMerchants);
      expect(controller.getMerchants()).toEqual(mockMerchants);
    });
  });

  describe('get one', () => {
    it('return the list"', () => {
      jest.spyOn(service, 'getMerchant').mockImplementation(() => mockMerchants[1]);
      expect(controller.getMerchant(2)).toEqual(mockMerchants[1]);
    });
  });
});
