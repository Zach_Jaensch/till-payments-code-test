import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { Merchant } from '../../Models/Merchant';
import { MerchantsService } from '../../Services/merchants/service';

@Controller('merchants')
export class MerchantsController {
  constructor(private readonly service: MerchantsService) {}

  @Get()
  getMerchants(): Merchant[] {
    return this.service.getMerchants();
  }

  @Get(':id')
  getMerchant(@Param('id', ParseIntPipe) id: number): Merchant {
    return this.service.getMerchant(id);
  }
}
