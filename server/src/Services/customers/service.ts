import { Injectable } from '@nestjs/common';
import { Customer, createCustomer, updateCustomer } from '../../Models/Customer';
import * as initalCustomers from '../../../mockCustomers.json'

@Injectable()
export class CustomersService {
  customers: Customer[] = initalCustomers.map((customer) => new Customer({
    id: Number(customer.id),
    merchantId: Number(customer.merchantId),
    name: customer.name
  }))

  getCustomers(): Customer[] {
    return this.customers;
  }

  getCustomer(idToFind: number): Customer {
    const customer = this.customers.find((customerInstance) => customerInstance.id === idToFind)

    return customer;
  }

  addCustomer(newCustomer: createCustomer): Customer[] {
    const newId = this.customers[this.customers.length - 1].id + 1
    this.customers.push(new Customer({ id: newId, ...newCustomer}))
    return this.customers;
  }

  updateCustomer(idToFind: number, customerUpdate: updateCustomer): Customer {
    const customer = this.customers.find((customerInstance) => customerInstance.id === idToFind)
    customer.update(customerUpdate)

    return customer;
  }

  deleteCustomer(idToFind: number): Customer[] {
    const customerIndex = this.customers.findIndex((customerInstance) => customerInstance.id === idToFind)

    this.customers.splice(customerIndex, 1)

    return this.customers;
  }
}
