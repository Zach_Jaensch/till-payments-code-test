import { CustomersService } from './service';
import { Customer } from '../../Models/Customer';

const mockCustomerData = [
  {
    "id": 1,
    "merchantId": 1,
    "name": "Alfred Jones"
  },
  {
    "id": 2,
    "merchantId": 1,
    "name": "Sally Brian"
  },
]

const mockCustomers = mockCustomerData.map((customer) => new Customer(customer))

jest.mock('../../../mockCustomers.json', () => {
  return [
    {
      "id": 1,
      "merchantId": 1,
      "name": "Alfred Jones"
    },
    {
      "id": 2,
      "merchantId": 1,
      "name": "Sally Brian"
    },
  ]
})


describe('CustomersController', () => {
  let service: CustomersService

  beforeEach(() => {
    service = new CustomersService()
  })
  describe('getCustomers', () => {
    it('return the list"', () => {
      expect(service.getCustomers()).toEqual(mockCustomers);
    });
  });

  describe('getCustomer', () => {
    it('return the list"', () => {
      expect(service.getCustomer(2)).toEqual(mockCustomers[1]);
    });
  });

  describe('addCustomer', () => {
    it('return the list"', () => {
      const newCustomer = {
        "merchantId": 1,
        "name": "James Frank"
      }

      const expected = [...mockCustomers, new Customer({
        "id": 3,
        "merchantId": 1,
        "name": "James Frank"
      })]


      expect(service.addCustomer(newCustomer)).toEqual(expected);
    });
  });

  describe('updateCustomer', () => {
    it('return the list"', () => {
      const updateCustomer = {
        "name": "James Frank"
      }
      
      const expected = new Customer({
        "id": 1,
        "merchantId": 1,
        "name": "James Frank"
      })


      expect(service.updateCustomer(1, updateCustomer)).toEqual(expected);
    });
  });

  describe('removeCustomer', () => {
    it('return the list"', () => {

      expect(service.deleteCustomer(1)).toEqual([mockCustomers[1]]);
    });
  });
});
