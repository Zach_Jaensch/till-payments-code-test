import { MerchantsService } from './service';
import { Merchant } from '../../Models/Merchant';
import { Transaction } from '../../Models/Transaction';

const mockMerchantData = [
  {
    "id": "001",
    "name": "Barney's Deals on Wheels",
    "isTrading": true,
    "currency": "AUD",
    "transactions": [
      {
        "id": "001",
        "amount": 45000,
        "description": "Minor Service",
        "ccLastFour": "4234",
        "ccExpiry": "11/04",
        "ccToken": "GH:D54356JJ$#2>GERG",
        "customerId": "001",
        "date": "2020-09-21 04:56:58.701972"
      },
    ]
  },
  {
    "id": "002",
    "name": "Nurburgring Laps",
    "isTrading": true,
    "currency": "EURO",
    "transactions": [
      {
        "id": "001",
        "amount": 2000,
        "description": "1 Lap",
        "ccLastFour": "4234",
        "ccExpiry": "11/04",
        "ccToken": "GH:D54356JJ$#2>GERG",
        "customerId": "004",
        "date": "2020-10-21 02:56:58.701972"
      },
    ]
  },
]

const mockMerchants = mockMerchantData.map((merchant) => {
  return (
    new Merchant({
      ...merchant,
      id: Number(merchant.id),
      transactions: merchant.transactions.map((transaction) => {
        return (
          new Transaction({
            ...transaction,
            id: Number(transaction.id),
            date: new Date(transaction.date)
          })
        )
      })
    })
  )
})

jest.mock('../../../mockMerchants.json', () => {
  return [
    {
      "id": "001",
      "name": "Barney's Deals on Wheels",
      "isTrading": true,
      "currency": "AUD",
      "transactions": [
        {
          "id": "001",
          "amount": 45000,
          "description": "Minor Service",
          "ccLastFour": "4234",
          "ccExpiry": "11/04",
          "ccToken": "GH:D54356JJ$#2>GERG",
          "customerId": "001",
          "date": "2020-09-21 04:56:58.701972"
        },
      ]
    },
    {
      "id": "002",
      "name": "Nurburgring Laps",
      "isTrading": true,
      "currency": "EURO",
      "transactions": [
        {
          "id": "001",
          "amount": 2000,
          "description": "1 Lap",
          "ccLastFour": "4234",
          "ccExpiry": "11/04",
          "ccToken": "GH:D54356JJ$#2>GERG",
          "customerId": "004",
          "date": "2020-10-21 02:56:58.701972"
        },
      ]
    },
  ]
})


describe('CustomersController', () => {
  let service: MerchantsService

  beforeEach(() => {
    service = new MerchantsService()
  })
  describe('getMerchants', () => {
    it('return the list"', () => {
      expect(service.getMerchants()).toEqual(mockMerchants);
    });
  });

  describe('getMerchant', () => {
    it('return the list"', () => {
      expect(service.getMerchant(2)).toEqual(mockMerchants[1]);
    });
  });
});
