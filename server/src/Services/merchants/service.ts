import { Injectable } from '@nestjs/common';
import { Merchant, createMerchant, updateMerchant } from '../../Models/Merchant';
import * as initalMerchants from '../../../mockMerchants.json'
import { Transaction } from '../../Models/Transaction';

@Injectable()
export class MerchantsService {
  Merchants: Merchant[] = initalMerchants.map((merchant) => {
    
    return (
      new Merchant({
        ...merchant,
        id: Number(merchant.id),
        transactions: merchant.transactions.map((transaction) => {
          return (
            new Transaction({
              ...transaction,
              id: Number(transaction.id),
              date: new Date(transaction.date),
              customerId: Number(transaction.customerId),
            })
          )
        })
      })
    )
  })

  getMerchants(): Merchant[] {
    return this.Merchants;
  }

  getMerchant(idToFind: number): Merchant {
    const Merchant = this.Merchants.find((MerchantInstance) => MerchantInstance.id === idToFind)

    return Merchant;
  }
}
